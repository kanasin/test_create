const persons = [
    {
        "id": "1001",
        "firstname": "Luke",
        "lastname": "Skywalker",
        "company": "Walt Disney",
        "salary": [40000,44000,48400]
    },

    {
        "id": "1002",
        "firstname": "Tony",
        "lastname": "Stark",
        "company": "Marvel",
        "salary": [1000000,1100000,1210000]
    },

    {
        "id": "1003",
        "firstname": "Somchai",
        "lastname": "Jaidee",
        "company": "Love2work",
        "salary": [20000,22000,24200]
    },

    {
        "id": "1004",
        "firstname": "Monkey D",
        "lastname": "Luffee",
        "company": "One Piece",
        "salary": [900000,990000,1089000]
    }
];

let header;
let tbody;
let salary;
for (const key in persons) {
    if(key==0){
        header += "<thead><tr>";
        tbody += "<tbody><tr>";
        for (const key2 in persons[key]) {
            if(key2!=='company'){
                header += "<th>"+key2+"</th>";
                if(key2==='salary'){
                    salary = "<ol>";
                    for(key3 in persons[key][key2]){
                        salary += "<li>"+persons[key][key2][key3]+"</li>";
                    }
                    salary += "</ol>";
                    tbody += "<td>"+salary+"</td>";
                    salary = "";
                }else{
                    tbody += "<td>"+persons[key][key2]+"</td>";
                }
            }
        }
        tbody += "</tr>";
        header += "</tr></thead>";
    }else{
        tbody += "<tr>";
        for (const key2 in persons[key]) {
            if(key2!=='company'){
                if(key2==='salary'){
                    salary += "<ol>";
                    for(key3 in persons[key][key2]){
                        salary += "<li>"+persons[key][key2][key3]+"</li>";
                    }
                    salary += "</ol>";
                    tbody += "<td>"+salary+"</td>";
                    salary = "";
                }else{
                    tbody += "<td>"+persons[key][key2]+"</td>";
                }
            }
        }
        tbody += "</tr>";
    }
    if(key==persons.length-1){
        tbody += "</tbody>";
    }
        
}
console.log(tbody);
$('#table_json').html(header+tbody);